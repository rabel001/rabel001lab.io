(function() {
	var target = document.querySelector('header');
	var width = target.offsetWidth;

	function sleep(time) {
		return new Promise(function(resolve) { 
			setTimeout(resolve, time); 
		});
	}

	var elements = {
		evens: ['#ra-menu', '#left-line', '#exit-btn'],
		odds: ['#menu-btn']
	}

	function checksize(width, instant) {
        var timeout = 150;
        var theframe = document.querySelector('.theframe');
		if(width <= 900) {
			var promise = new Promise(function(resolve) {
				elements.evens.forEach(function(e) {
                    var el = document.querySelector(e);
					if(instant) {
						el.classList.add('hidden');
						el.classList.remove('show');
                        el.classList.add('hide');
                        resolve(true);
					} else {
						el.classList.remove('show');
						el.classList.add('hide');
						sleep(timeout).then(function() {
                            el.classList.add('hidden');
                            theframe.style.width = '100%';
                            resolve(true);
						});
					}
				});
			});
			promise.then(function() {
				elements.odds.forEach(function(e) {
                    var el = document.querySelector(e);
					if(instant) {
						el.classList.remove('hidden');
						el.classList.remove('hide');
						el.classList.add('show');
					} else {
						el.classList.remove('hidden');
						sleep(timeout).then(function() {
							el.classList.remove('hide');
                            el.classList.add('show');
						});
					}
				});
			});
		} else {
			var promise = new Promise(function(resolve) {
                elements.odds.forEach(function(e) {
					var el = document.querySelector(e);
					if(instant) {
						el.classList.add('hidden');
						el.classList.remove('show');
                        el.classList.add('hide');
                        resolve(true);
                    }
                    else {
                        el.classList.add('hidden');
                        sleep(timeout).then(function() {
                            el.classList.add('hide');
                            el.classList.remove('show');
                            resolve(true);
                        });
                    }
				});
			});
			promise.then(function() {
				elements.evens.forEach(function(e) {
					var el = document.querySelector(e);
					if(instant) {
                        el.classList.remove('hidden');
						el.classList.add('show');
                        el.classList.remove('hide');
					} else {
                        theframe.style.width = 'calc(100% - 270px)';
						el.classList.remove('hidden');
						sleep(timeout).then(function() {
							el.classList.add('show');
                            el.classList.remove('hide');
						});
					}
				});
			});
		}
	}
	
	checksize(width, true);
	
	window.onresize = function() {
		checksize(document.querySelector('header').offsetWidth, false);
	}

	document.querySelector('#menu-btn').addEventListener('click', function() {
		checksize(1000, false);
	});

	document.querySelector('#exit-btn').addEventListener('click', function() {
		checksize(900, false);
	});
})();



function toggleInfo(hide) {
	if(hide) {
		if(!document.querySelector('.info').classList.contains('hidden')) {
			document.querySelector('.info').classList.add('hidden');
			document.querySelector('.theframe').classList.remove('hidden');
		}
	} else {
		if(document.querySelector('.info').classList.contains('hidden')) {
			document.querySelector('.info').classList.remove('hidden');
			document.querySelector('.theframe').classList.add('hidden');
		}
	}
}

document.querySelector('main').addEventListener('click', function(evt) {
	if(evt.target.classList.contains('link')) {
		document.querySelector('#main').click();
	} else {
		if(evt.target.tagName === "A") {
			if(evt.target.parentElement.classList.contains('nav-item')) {
				document.querySelectorAll('.nav-item').forEach(function(e) {
					e.classList.remove('active');
				});
				if(evt.target.parentElement.classList.contains('nav-item')) {
					evt.target.parentElement.classList.add('active');
					if(evt.target.parentElement.parentElement.parentElement.classList.contains('nav-item')) {
						evt.target.parentElement.parentElement.parentElement.classList.add('active');
					}
				}
			}
		}
	}
});

function setFrame(id) {
	document.querySelector('.theframe').src = document.querySelector(id).href;
	toggleInfo(true);
}

document.querySelector('#pass-gen-pop').addEventListener('click', function() {
	setFrame('#pass-gen');
});
document.querySelector('#oload-vwr-pop').addEventListener('click', function() {
	setFrame('#oload-vwr');
});
document.querySelector('#openpgpjs-pop').addEventListener('click', function() {
	setFrame('#openpgpjs')
});
document.querySelector('#rsa-txt-pop').addEventListener('click', function() {
	setFrame('#rsa-txt');
});
document.querySelector('#crypt-one-pop').addEventListener('click', function() {
	setFrame('#crypt-one')
});
document.querySelector('#crypt-two-pop').addEventListener('click', function() {
	setFrame('#crypt-two');
});
document.querySelector('#crypt-three-pop').addEventListener('click', function() {
	setFrame('#crypt-three')
});
document.querySelector('#main').addEventListener('click', function() {
	toggleInfo(false);
	document.querySelector('.theframe').src = "";
});